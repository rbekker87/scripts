#!/bin/bash

# functions

create_yum_repo() {
    wget -O /etc/yum.repos.d/mongodb.repo https://gitlab.com/rbekker87/configs/raw/master/repos/rhel/mongodb-org-2.3.repo 
}

install_yum_packages() {
    yum update -y
    yum install mongodb-org* -y
    mkdir -p /data/db
}

start_service() {
    /etc/init.d/mongod restart
}


if [ "$(which yum 2> /dev/null | wc -l)" == "1" ] ;
    then 
        echo 'Dected RHEL Based OS';
        create_yum_repo
        install_yum_packages
        start_service

    elif [ "$(which apt 2> /dev/null | wc -l)" == "1" ] ;
        then 
            echo 'Detected Debian Based OS';

    else
        echo 'not supported'

fi
#!/bin/bash

cd /tmp/
curl -O http://downloads.rclone.org/rclone-current-linux-amd64.zip
yum install unzip -y
unzip rclone-current-linux-amd64.zip
cd /tmp/rclone-v1*
cp rclone /usr/sbin/
chown root:root /usr/sbin/rclone
chmod 755 /usr/sbin/rclone
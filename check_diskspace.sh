#!/bin/bash

EMAIL="email@domain.com"
SUBJECT=""
HOSTNAME=`hostname`

CHECK=`df -h  | grep ploop | awk '{print $5}' | cut -d'%' -f1`

function sendAlert() {
    echo "Diskspace Metric: $CHECK" | mail -s "ALERT: LOW DISKSPACE on $HOSTNAME" $EMAIL
}

if [ $CHECK -gt "80" ] ;
    then sendAlert ;
    else exit 0;
fi


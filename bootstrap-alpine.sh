#!/bin/bash
echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" > /etc/apk/repositories 
echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories 
echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories 
apk update --no-cache
apk add --no-cache linux-headers build-base bash git ca-certificates openssl openssl-dev curl wget unzip
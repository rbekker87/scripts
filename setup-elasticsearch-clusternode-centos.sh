#!/bin/bash
# my testing was done with LXD/LXC so I had to set this on my hypervisor
# sysctl -w vm.max_map_count=262144

# set elasticsearch cluster name:
ES_CLUSTER_NAME="myescluster"

# set elasticsearch nodes ip/dns that will be part of the elasticsearch cluster
ES_NODES='["10.0.0.2", "10.0.0.3"]'

# create backup directory for elasticsearch config
mkdir /opt/backups/elasticsearch -p

# install elasticsearch dependencies
yum update -y
yum install net-tools curl -y
yum install java-1.8.0-openjdk -y

# create elasticsearch repository
cat > /etc/yum.repos.d/es.repo << EOF
[elasticsearch-5.x]
name=Elasticsearch repository for 5.x packages
baseurl=https://artifacts.elastic.co/packages/5.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOF

# install elasticsearch
yum update -y
yum install elasticsearch -y

# stop elasticsearch and backup config
systemctl stop elasticsearch
mv /etc/elasticsearch/elasticsearch.yml /opt/backups/elasticsearch/elasticsearch.yml.BAK

# bootstrap elasticsearch config with defined values
cat > /etc/elasticsearch/elasticsearch.yml << EOF
cluster.name: $ES_CLUSTER_NAME
node.name: \${HOSTNAME}
network.host: 0.0.0.0
discovery.zen.ping.unicast.hosts: $ES_NODES
EOF

# start elasticsearch
systemctl enable elasticsearch
systemctl restart elasticsearch
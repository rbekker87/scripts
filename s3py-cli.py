#!/usr/bin/python3.4
# https://github.com/scality/S3

import sys
import boto3
from boto.s3.connection import S3Connection, OrdinaryCallingFormat

client = boto3.client(
    's3',
    aws_access_key_id='accessKey1',
    aws_secret_access_key='verySecretKey1',
    endpoint_url='http://localhost:8000'
)


def listBuckets():
    list = client.list_buckets()
    lists = list['Buckets']
    for x in lists:
        print(x['Name'])

def createBucket():
    client.create_bucket(Bucket="testbucket10")

if sys.argv[1] == "list":
    listBuckets()

elif sys.argv[1] == "cb":
    createBucket()
    print("Bucket: has been created.")

else:
    print("None Selected")


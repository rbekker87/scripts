#!/bin/bash
# http://dev-notes.eu/2016/08/persistent-iptables-rules-in-ubuntu-16-04-xenial-xerus/

# INSTALL
# apt-get install iptables-persistent -y
# service netfilter-persistent start
# invoke-rc.d netfilter-persistent save
#

# README:
# save rules:
# $ iptables-save > /etc/iptables/rules.v4
# $ netfilter-persistent save

# direct inter mysql traffic
iptables -I INPUT -s 1.2.3.4 -p tcp --dport 3306 -j ACCEPT
iptables -I INPUT -s 2.3.4.5 -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -p tcp --dport 3306 -j DROP

# direct inter mongodb traffic
iptables -I INPUT -s 1.2.3.4 -p tcp --dport 27017 -j ACCEPT
iptables -I INPUT -s 2.3.4.5 -p tcp --dport 27017 -j ACCEPT
iptables -A INPUT -p tcp --dport 27017 -j DROP

# iptables-save > /etc/iptables/rules.v4

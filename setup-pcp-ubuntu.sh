apt update
apt install apt-transport-https -y
curl --silent 'https://bintray.com/user/downloadSubjectPublicKey?username=pcp' | sudo apt-key add -
echo "deb https://dl.bintray.com/pcp/xenial xenial main" | sudo tee -a /etc/apt/sources.list.d/pcp.list > /dev/null
apt update
apt install pcp pcp-webapi
/etc/init.d/pmcd start
/etc/init.d/pmlogger restart
/etc/init.d/pcp restart
/etc/init.d/pmproxy restart
/etc/init.d/pmwebd restart
netstat -naptu | grep pmwebd
#!/bin/bash

if [ -z $1 ];
    echo "Usage: $0 (all|networking)"
    elif [ $1 = "all" ];
        then 
            all;
    elif [ $1 = "networking" ];
        then
            networking;
        else
            exit 0;
fi
        

function all() {
  yum update -y
  yum install wget telnet nc tcpdump -y
}

function networking() {
  yum update -y
  yum install tcpdump nload vnstat -y
}
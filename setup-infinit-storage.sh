#!/bin/bash

USER=""

debian(){
sudo apt-get install fuse -y
sudo usermod -aG fuse $USER
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3D2C3B0B
sudo apt-get install software-properties-common apt-transport-https
sudo add-apt-repository "deb https://debian.infinit.sh/ trusty main"
sudo apt-get update
sudo apt-get install infinit -y
}
#!/bin/bash

# variables
domain="domain.com"

# generate a private key for the server
openssl genrsa -des3 -out mail.$domain.key 2048

# create the certificate signing request
openssl req -new -key mail.$domain.key -out mail.$domain.csr

# create a self signed key
openssl x509 -req -days 365 -in mail.$domain.csr -signkey mail.$domain.key -out mail.$domain.crt

# remove the password from the private certificate
openssl rsa -in mail.$domain.key -out mail.$domain.key.nopass
mv mail.$domain.key.nopass mail.$domain.key

# make ourself trusted certificate authority
openssl req -new -x509 -extensions v3_ca -keyout cakey.pem -out cacert.pem -days 3650
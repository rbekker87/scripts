#!/bin/bash

# Docs: http://docs.ansible.com/ansible/intro_installation.html

detect_os() {

if [ $(which apt | wc -l) = "1" ] 
  then OS="DEBIAN"
  elif [ $(which yum | wc -l) = "1" ]
    then OS="RHEL"
  else
    exit 0;
fi

}

setup_ansible_debian() {

sudo apt-get install software-properties-common -y
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible -y

}

detect_os()

if [ $OS == "DEBIAN"] 
  then setup_ansible_debian
  ## still need rhel setup
  else
    exit 0
fi

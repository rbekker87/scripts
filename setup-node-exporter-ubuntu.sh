#!/bin/bash
set -ex
NODE_EXPORTER_VERSION="0.17.0"

useradd --no-create-home --shell /bin/false node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION:-0.17.0}/node_exporter-${NODE_EXPORTER_VERSION:-0.17.0}.linux-amd64.tar.gz
tar -xf node_exporter-${NODE_EXPORTER_VERSION:-0.17.0}.linux-amd64.tar.gz

cp node_exporter-${NODE_EXPORTER_VERSION:-0.17.0}.linux-amd64/node_exporter /usr/local/bin
chown node_exporter:node_exporter /usr/local/bin/node_exporter
rm -rf node_exporter-${NODE_EXPORTER_VERSION:-0.17.0}.linux-amd64*

cat > /etc/systemd/system/node_exporter.service << EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl restart node_exporter
systemctl enable node_exporter

iptables -I INPUT -s ${PROMETHEUS_IP:-127.0.0.1} -p tcp --dport 9100 -j ACCEPT
iptables -A INPUT -p tcp --dport 9100 -j DROP
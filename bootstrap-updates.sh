#!/bin/bash
# setup updates depending on distribution

YUM=`which yum 2> /dev/null | wc -l`
APT=`which apt 2> /dev/null | wc -l`

# functions
updateSystem() {
    $PKG_MGR update -y
    $PKG_MGR upgrade -y
}

bootstrapDebianUpdates() {
    apt-get install apt-transport-https ca-certificates curl software-properties-common -y
}

# os detection
if [ $YUM = "1" ] ;
    then export PKG_MGR="yum"
         updateSystem

    elif [ $APT = "1" ] ;
        then export PKG_MGR="apt-get"
             updateSystem
             bootstrapDebianUpdates

    else
        echo 'Error: No Supported Distributions Found'

fi

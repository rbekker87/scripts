#!/bin/bash

service nginx stop
/opt/certbot/certbot-auto certonly  --agree-tos --config /opt/letsencrypt/cli.ini
service nginx start

# cli.ini
## rsa-key-size = 4096
## email = mail@domain.com
## domains = domain.com, www.domain.com
## text = True
## authenticator = standalone

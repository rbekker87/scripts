# writes data to disk and also gz files
# still need to clean the script

import datetime
import random
import gzip
import uuid

shops = ['Game', 'Chekcers', 'OK', 'Makro', 'Friendly Grocer']
payment_types = ['Credit', 'Cash', 'Debit', 'Account']
products = ['Food', 'Clothes', 'Cutlery', 'Alcohol', 'Sweets', 'Furniture', 'Gifts']


for x in range(10):

    data_date = (str(random.randint(2012,2013)) + '-' + str(random.randint(01,12)) + '-' + str(random.randint(01,28)))
    data_time = (str(random.randint(10,19)) + ':' + str(random.randint(10,55)))
    data_price = (str(random.randint(10,250)) + '.' + str(random.randint(10,99)))

    hashv = uuid.uuid4().hex
    file_obj = open(hashv + '.txt', 'a')
    for y in range(70000):
        file_obj.write(data_date + '\t' + data_time + '\t' + random.choice(shops) + '\t' + random.choice(products) + '\t' + data_price + '\t' + random.choice(payment_types) + '\n')
    file_obj.close()
    gzip_a = open(hashv + '.txt')
    gzip_b = gzip.open(hashv + '.txt' + '.gz', 'wb')
    gzip_b.writelines(gzip_a)
    gzip_b.close()
    gzip_a.close()


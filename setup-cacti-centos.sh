#!/bin/bash

yum install mysql-server mysql php-mysql php-pear php-common php-gd php-devel php php-mbstring php-cli php-snmp php-pear-Net-SMTP php-mysql httpd -y
mysql -u root -p -e 'create database cacti;'
mysql -u root -p -e 'GRANT ALL ON cacti.* TO cacti@localhost IDENTIFIED BY "zYn95ph43zYtq";'
mysql -u root -p -e 'FLUSH privileges;'

yum install net-snmp-utils php-snmp net-snmp-libs -y

cd /etc/snmp/
mv snmpd.conf old-snmpd-conf
wget https://gitlab.com/rbekker87/configs/raw/master/snmp/snmpd.conf.txt
mv snmpd.conf.txt snmpd.conf

/etc/init.d/snmpd start
chkconfig snmpd on

yum install cacti -y

cactisql=`rpm -ql cacti | grep cacti.sql`

mysql -u cacti -pzYn95ph43zYtq cacti < $cactisql

wget -O /etc/cacti/db.php https://gitlab.com/rbekker87/configs/raw/master/php/cacti-db.php
wget -O /etc/httpd/conf.d/cacti.conf https://gitlab.com/rbekker87/configs/raw/master/apache/cacti.conf

echo "*/5 * * * *     cacti   /usr/bin/php /usr/share/cacti/poller.php > /dev/null 2>&1" >> /etc/cron.d/cacti

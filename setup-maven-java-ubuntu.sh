#!/bin/bash

apt update && apt upgrade -y
add-apt-repository ppa:webupd8team/java
apt update

apt install oracle-java8-installer -y
cd /opt/
wget http://www-eu.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar -xvzf apache-maven-3.3.9-bin.tar.gz
mv apache-maven-3.3.9 maven 
 
cat > /etc/profile.d/mavenenv.sh << EOF
export M2_HOME=/opt/maven
export PATH=\${M2_HOME}/bin:\${PATH}
EOF

source /etc/profile.d/mavenenv.sh

mvn -V
#!/bin/bash

YUM=`which yum | wc -l`
APT=`which apt | wc -l`

if [ $YUM = "1" ] ;
    then echo 'Selecting YUM'

    elif [ $APT = "1" ] ;
        then echo 'Selecting APT'

    else
        echo 'not supported'

fi

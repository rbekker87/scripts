#!/bin/bash

# Postfix, Dovecot, PostfixAdmin, MySQL
# tag: mail-imp

# variables
GITURL="https://gitlab.com/rbekker87/configs/raw/master"
export FQDN=$(hostname -f)


function authentication() {
  export MYPASS=$(cat /proc/sys/kernel/random/uuid | cut -d'-' -f1)
  echo "MySQL Root Password: $MYPASS" >> ~/mypasswords.txt
}

function disable_selinux() {
  setenforce 0
  sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
}

# rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*
# rpm --import http://dag.wieers.com/rpm/packages/RPM-GPG-KEY.dag.txt

function get_repos() {
  cd /tmp
  rpm -Uvh http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm 
  rpm --import https://fedoraproject.org/static/0608B895.txt
}

function update () {
  yum update -y
}

function dependencies() {
  yum update -y
  yum install epel-release -y
  yum install yum-priorities -y
  yum groupinstall 'Development Tools' -y
  yum install wget ntp httpd mod_ssl mysql-server php php-mysql php-mbstring dovecot dovecot-mysql postfix amavisd-new spamassassin clamav clamd unzip bzip2 unrar perl-DBD-mysql php php-devel php-gd php-imap php-ldap php-mysql php-odbc php-pear php-xml php-xmlrpc php-pecl-apc php-mbstring php-mcrypt php-mssql php-snmp php-soap php-tidy curl curl-devel perl-libwww-perl ImageMagick libxml2 libxml2-devel mod_fcgid php-cli httpd-devel httpd-devel ruby ruby-devel mod_python openssl perl-DateTime-Format-HTTP perl-DateTime-Format-Builder -y
}

function update_spamassassin() {
  sa-update
}

function php_config() {
  sed -i 's/;error_reporting = E_ALL & ~E_DEPRECATED/error_reporting = E_ALL & ~E_NOTICE/' /etc/php.ini
  sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/' /etc/php.ini
}

function enable_services () {
  chkconfig --levels 235 dovecot on
  chkconfig --levels 235 mysqld on
  chkconfig --levels 235 postfix on
  chkconfig --levels 235 httpd on
  chkconfig --levels 235 amavisd on
  chkconfig --del clamd
  chkconfig --levels 235 clamd.amavisd on

function restart_services() {
  /etc/init.d/dovecot start
  /etc/init.d/mysqld start
  /etc/init.d/postfix restart
  /etc/init.d/httpd restart
}

function get_virus_defs() {
  /usr/bin/freshclam
}

function get_amavisd_config() {
  mv /etc/amavisd/amavisd.conf{,.bak}
  wget -O /etc/amavisd/amavisd.conf $GITURL/amavisd/amavisd.conf
}

function restart_amavisd() {
  /etc/init.d/amavisd start
  /etc/init.d/clamd.amavisd start
}

function set_mysql_pass() {
  mysqladmin -u root password $MYPASS
}

function set_postfix_configuration() {
  mv /etc/postfix/main.cf{,.bak}
  wget -O /etc/postfix/main.cf $GITURL/postfix/mail-imp-postfix-main.cf
  postconf -e "myhostname = $(hostname -f)" 
  wget -O /tmp/postfix.sql $GITURL/mysql/postfix/postfix-mail-imp.sql
  mysql -u root -p$MYPASS < /tmp/postfix.sql
  wget -O /etc/postfix/mysql_virtual_alias_maps.cf $GITURL/postfix/mail-imp-mysql_virtual_alias_maps.cf 
  wget -O /etc/postfix/mysql_virtual_domains_maps.cf $GITURL/postfix/mail-imp-mysql_virtual_domains_maps.cf
  wget -O /etc/postfix/mysql_virtual_mailbox_maps.cf $GITURL/postfix/mail-imp-mysql_virtual_mailbox_maps.cf
  wget -O /etc/postfix/mysql_virtual_mailbox_limit_maps.cf $GITURL/postfix/mail-imp-mysql_virtual_mailbox_limit_maps.cf
  wget -O /etc/postfix/mysql_relay_domains_maps.cf $GITURL/postfix/mail-imp-mysql_relay_domains_maps.cf
}

function set_dovecot_configuration() {
  mv /etc/dovecot/dovecot.conf{,.bak}
  wget -O /etc/dovecot/dovecot.conf $GITURL/dovecot/mail-imp-dovecot.conf
  wget -O /etc/dovecot/dovecot-sql.conf $GITURL/dovecot/mail-imp-dovecot-sql.conf
}

function prepare_mail_dirs() {
  mkdir /var/vmail
  useradd vmail -r -u 5000 -g mail -d /var/vmail -s /sbin/nologin
  groupadd vmail -g 5001
  chmod -R 777 /var/vmail/
  chown -R vmail:vmail /var/vmail/
  touch /var/log/dovecot.log
  chmod 777 /var/log/dovecot.log
  chown mysql:mysql /etc/postfix/mysql_*
}

function set_httpd_configuration() {
  wget -O /etc/httpd/conf.d/mailmanage.conf $GITURL/apache/mail-imp-mailmanager.conf
  mkdir /var/www/mailmanage/
  cd /tmp/
  wget -O postfixadmin.tar.gz https://dl.dropboxusercontent.com/u/31991539/repo/packages/postfixadmin-2.3.6.tar.gz
  tar -xf postfixadmin.tar.gz
  cp -r /tmp/postfixadmin-2.3.6/* /var/www/mailmanage/
}

function set_ssl_configuration() {
  openssl genrsa -des3 -out $FQDN.key 2048
  openssl req -new -key $FQDN.key -out $FQDN.csr
  openssl x509 -req -days 365 -in $FQDN.csr -signkey $FQDN.key -out $FQDN.crt
  openssl rsa -in $FQDN.key -out $FQDN.key.nopass
  yes | mv $FQDN.key.nopass $FQDN.key
  openssl req -new -x509 -extensions v3_ca -keyout cakey.pem -out cacert.pem -days 3650
  chmod 600 $FQDN.key
  chmod 600 cakey.pem
  ln -s /etc/pki/tls/private /etc/ssl/private
  mv $FQDN.key /etc/ssl/private/
  mv $FQDN.crt /etc/ssl/certs/
  mv cakey.pem /etc/ssl/private/
  mv cacert.pem /etc/ssl/certs/
}

function update_postfix_configurationv2() {
  postconf -e 'smtpd_use_tls = yes'
  postconf -e 'smtpd_tls_auth_only = no'
  postconf -e 'smtpd_tls_key_file = /etc/ssl/private/mail.domain.tld.key'
  postconf -e 'smtpd_tls_cert_file = /etc/ssl/certs/mail.domain.tld.crt'
  postconf -e 'smtpd_tls_CAfile = /etc/ssl/certs/cacert.pem'
  postconf -e 'tls_random_source = dev:/dev/urandom'
  postconf -e "myhostname = $FQDN"
}

if [[ -f "/tmp/.install.lock" ]];

  then
        echo "lock present";
        exit 0;
  else
        authentication
        disable_selinux
        get_repos
        update
        dependencies
        update_spamassassin
        php_config
        restart_services
        enable_services
        get_virus_defs
        get_amavisd_config
        restart_amavisd
        set_mysql_pass
        set_postfix_configuration
        set_dovecot_configuration
        prepare_mail_dirs
        set_httpd_configuration
        set_ssl_configuration
        update_postfix_configurationv2
        restart_services

        echo "Configure Postfixadmin"
        echo "remember to set domain_path to YES for domain.com/user"
        echo "Passwords: ~/mypasswords.txt"

fi
#!/bin/bash
apt update
apt install openjdk-8-jdk -y
apt install wget openssl -y
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
echo 'deb https://pkg.jenkins.io/debian-stable binary/' | tee -a /etc/apt/sources.list
apt update
apt install jenkins -y
systemctl enable jenkins
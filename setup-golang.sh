#!/bin/bash
# setup updates depending on distribution

YUM=`which yum 2> /dev/null | wc -l`
APT=`which apt 2> /dev/null | wc -l`

# functions
updateSystem() {
    $PKG_MGR update -y
    $PKG_MGR upgrade -y
}

goDebian() {
    sudo apt-get install golang gccgo mercurial -y
    export GOPATH=$HOME/go  
    PATH=$PATH:$HOME/.local/bin:$HOME/bin:$GOPATH/bin 
}

goDebianSrc() {
    apt install build-essential -y
    curl -SLsf https://dl.google.com/go/go1.12.14.linux-amd64.tar.gz > go.tgz
    rm -rf /usr/local/go
    mkdir -p /usr/local/go
    tar -xvf go.tgz -C /usr/local/go/ --strip-components=1
    GOPATH=$HOME/go/
    PATH=$PATH:/usr/local/go/bin/
    go version
}

goCentos() {
    yum install golang -y
    export GOPATH=$HOME/go  
    PATH=$PATH:$HOME/.local/bin:$HOME/bin:$GOPATH/bin 
}

# os detection
if [ $YUM = "1" ] ;
    then export PKG_MGR="yum"
         updateSystem
         goCentos

    elif [ $APT = "1" ] ;
        then export PKG_MGR="apt-get"
             updateSystem
             goDebianSrc

    else
        echo 'Error: No Supported Distributions Found'

fi
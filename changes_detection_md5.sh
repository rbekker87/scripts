#!/bin/bash

RESOLV_HASH="09f15a00b6ce0be250c33a178d75cb30"
RESOLV_CHECK=$(md5sum /etc/resolv.conf | awk '{print $1}')


function revert_resolv() {
    cat /etc/resolv.conf.template > /etc/resolv.conf
}

if [ "$RESOLV_CHECK" = "$RESOLV_HASH" ] ;
    then
        exit 0 ;
    else
        revert_resolv ;
fi

#!/bin/bash
#
# INFO:
#  - dumps information to /tmp/systemcheck-$TIMESTAMP.log
#

#BUCKETNAME="s3bucketname"
#INSTANCEID=`curl http://169.254.169.254/latest/meta-data/instance-id`
#TIMESTAMP=`date +%F-%T`
TIMESTAMP=`date +%FT%H-%M`

cd /var/log/
echo "Date" > systemcheck-$TIMESTAMP.log
echo "====" >> systemcheck-$TIMESTAMP.log
date >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Up Time" >> systemcheck-$TIMESTAMP.log
echo "==========" >> systemcheck-$TIMESTAMP.log
uptime >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Whats running" >> systemcheck-$TIMESTAMP.log
echo "=============" >> systemcheck-$TIMESTAMP.log
ps auxf --width=200 >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Top CPU users" >> systemcheck-$TIMESTAMP.log
echo "=============" >> systemcheck-$TIMESTAMP.log
ps auxwww --sort -%cpu | head -10 >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Top memory users" >> systemcheck-$TIMESTAMP.log
echo "================" >> systemcheck-$TIMESTAMP.log
ps auxwww --sort -rss | head -10  >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Kernel Messages" >> systemcheck-$TIMESTAMP.log
echo "===============" >> systemcheck-$TIMESTAMP.log
dmesg | tail -n 25 >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Active Connections" >> systemcheck-$TIMESTAMP.log
echo "==================" >> systemcheck-$TIMESTAMP.log
netstat -n -A  inet | grep -v "127.0.0.1" >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Listening Ports" >> systemcheck-$TIMESTAMP.log
echo "===============" >> systemcheck-$TIMESTAMP.log
netstat -tulpn >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Internet Breakout" >> systemcheck-$TIMESTAMP.log
echo "=================" >> systemcheck-$TIMESTAMP.log
curl -s -I --connect-timeout 1 --max-time 5 http://aws.amazon.com | head >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Latest SSH Access" >> systemcheck-$TIMESTAMP.log
echo "=================" >> systemcheck-$TIMESTAMP.log
tail -n 10 /var/log/secure | grep -v sudo >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "IO Statistics" >> systemcheck-$TIMESTAMP.log
echo "=============" >> systemcheck-$TIMESTAMP.log
iostat -x 1 2 >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Memory Usage" >> systemcheck-$TIMESTAMP.log
echo "============" >> systemcheck-$TIMESTAMP.log
free -m >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Trend Memory" >> systemcheck-$TIMESTAMP.log
echo "============" >> systemcheck-$TIMESTAMP.log
vmstat 1 5 >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Disk Space" >> systemcheck-$TIMESTAMP.log
echo "==========" >> systemcheck-$TIMESTAMP.log
df -h >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Whats utilizing the Disk" >> systemcheck-$TIMESTAMP.log
echo "========================" >> systemcheck-$TIMESTAMP.log
du -mxS / | sort -n | tail >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log
echo "Network Stats" >> systemcheck-$TIMESTAMP.log
echo "=============" >> systemcheck-$TIMESTAMP.log
netstat -s -e >> systemcheck-$TIMESTAMP.log
echo " " >> systemcheck-$TIMESTAMP.log

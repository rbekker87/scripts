#!/bin/bash
rm -rf /etc/apt/apt.conf.d/50unattended-upgrades.ucf-dist
apt update && apt-upgrade -y
apt install docker.io docker-compose -y
apt install -qy python-pip --no-install-recommends
pip install pip --upgrade
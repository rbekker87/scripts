#!/usr/bin/env bash

mongo_user=user
mongo_pass=pass

for file in `ls | grep bson`
  do
    for collection in `echo $file | sed 's/.bson//g'`
  do
    mongorestore --host mongoid.mlab.com --port 59344 -u $mongo_user -p $mongo_pass -d rocketchat -c $collection $file
    sleep 2
  done
done
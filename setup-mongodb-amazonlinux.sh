#!/bin/bash

cat > /etc/yum.repos.d/mongodb-org-3.0.repo << EOF
[mongodb-org-3.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/3.0/x86_64/
gpgcheck=0
enabled=1
EOF

yum install mongodb-org-server mongodb-org-shell mongodb-org-tools mongodb-org-debuginfo  -y  --enablerepo=mongodb-org-3.0

/etc/init.d/mongod restart
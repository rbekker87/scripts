from sqlalchemy import Column, ForeignKey, Integer, String, create_engine 
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import relationship, sessionmaker
import random

Base = declarative_base()
 
class Person(Base):
    __tablename__ = 'test1'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    city = Column(String(250), nullable=False)
 
engine = create_engine('mysql://user:pass@mysql.domain.com/db', echo=False)
#mem_engine = create_engine('sqlite:///:memory:', echo=False)
#local_engine = create_engine('sqlite:///test.db', echo=False)


Base.metadata.create_all(engine)
Base.metadata.bind = engine
 
DBSession = sessionmaker(bind=engine)
session = DBSession()

names = ['james', 'john', 'peter', 'nomad', 'zak']
cities = ['bloem', 'pretoria', 'italy', 'frankfurt', 'dublin']

for x in range(10):
    request = Person(id=random.randint(10,100), name=random.choice(names), city=random.choice(cities))
    session.add(request)
    session.commit()
    print(request.id)

session.close()

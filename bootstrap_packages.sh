#!/bin/bash

#: Info this script sets up all the needed packages, which I usually use.
# 
# Variables:

YUM_BASED=`which yum | wc -l`
APT_BASED=`which apt | wc -l`

# Functions

detect_rhel (){
  echo "rhel detected" 
}

detect_debian() {
  echo "debian detected"
}

update_rhel () {
  #yum update -y 
  echo "update rhel"
}

update_debian() {
  echo "update debian based os"
  apt-get update
  apt-get upgrade -y
}

install_sysadmin_rhel (){
  yum intall tcpdump iotop htop nc nload lsof sysstat iostat strace iftop iptraf -y
  echo "install rhel based packages"
}

install_sysadmin_debian() {
 echo "install debain based packages"
 apt-get install --reinstall systemd dbus -y
 apt-get install software-properties-common telnet unzip tcpdump netcat wget curl python python-dev python-setuptools telnet htop gcc lsof sysstat bzip2 net-tools build-essential net-tools -y
}

install_lemp_debian() {
    apt-get install nginx -y
    apt-get install mysql-server -y
    apt-get install php-fpm php-mysql -y
    sed -i 's/;cgi.fix_pathinfo=0/cgi.fix_pathinfo=1/g' /etc/php/7.0/fpm/php.ini
    systemctl restart php7.0-fpm
    wget https://gitlab.com/rbekker87/configs/raw/master/nginx/nginx-php.conf
    mv nginx-php.conf /etc/nginx/sites-available/default
    systemctl reload nginx
}

# Statements

if [ "$YUM_BASED" = "1" ] 
    then 
        detect_rhel
        update_rhel 
        install_sysadmin_rhel 

elif [ "$APT_BASED" = "1" ] 
    then 
        detect_debian
        update_debian 
        install_sysadmin_debian 
else
    exit 0

fi 

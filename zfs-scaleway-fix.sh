#!/bin/bash
#
# zfs issue with Ubuntu16 on Scaleway
#

arch="$(uname -m)"
release="$(uname -r)"
upstream="${release%%-*}"
local="${release#*-}"

apt-get update
apt-get install libssl-dev -y
apt-get install zfsutils-linux -y
zcat /proc/config.gz > /boot/config-${upstream}
cd /tmp;  wget https://www.kernel.org/pub/linux/kernel/v4.x/linux-${upstream}.tar.xz && tar xf linux-${upstream}.tar.xz
cp -r /tmp/linux-${upstream} /lib/modules/${release}/build && cd /lib/modules/${release}/build/
cp /boot/config-${upstream} .config
make oldconfig
make prepare scripts
apt-get remove zfsutils-linux -y
apt-get install zfsutils-linux -y
cd /lib/modules/${release}/build && make -j4
dkms --verbose install spl/0.6.5.6
dkms --verbose install zfs/0.6.5.6
dkms status
modprobe zfs
zpool list
#!/bin/bash
# basic script to test if variable is empty

TEST="$1"

if [ -z "$TEST" ];
   then echo empty ;
   else echo $TEST ;
fi

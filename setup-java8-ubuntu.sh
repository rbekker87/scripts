#!/bin/bash

apt update && apt upgrade -y
apt install software-properties-common python-software-properties -y
add-apt-repository ppa:webupd8team/java
apt update
apt install oracle-java8-installer -y
echo 'JAVA_HOME="/usr/lib/jvm/java-8-oracle"' >> /etc/environment
sudo su -c 'source /etc/environment' root
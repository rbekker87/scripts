cat > /etc/apt/sources.list << EOF
deb http://httpredir.debian.org/debian          jessie         main
deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main contrib non-free

deb http://ftp.us.debian.org/debian/ jessie main contrib non-free
deb-src http://ftp.us.debian.org/debian/ jessie main contrib non-free

deb http://httpredir.debian.org/debian jessie-updates main contrib non-free
deb-src http://httpredir.debian.org/debian jessie-updates main contrib non-free
EOF

apt-get update && apt-get upgrade -y
apt-get install ca-certificates -y
update-ca-certificates
apt-get install build-essential apt-utils apt-transport-https -y
apt-get install libsqlite3-dev zlib1g-dev libncurses5-dev libgdbm-dev libbz2-dev  libssl-dev libdb-dev -y
apt-get install ssh openssh-server wget curl tcpdump netcat vim cron makepasswd openssl -y 
apt-get install python -y
apt-get install python-setuptools -y
easy_install pip
pip install virtualenv
 
/etc/init.d/exim4 stop    
apt-get remove exim4 -y